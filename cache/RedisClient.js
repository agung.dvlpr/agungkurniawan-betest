const Redis = require('ioredis');

const url = `redis://${process.env.REDIS_USERNAME}:${process.env.REDIS_PASSWORD}@${process.env.REDIS_HOSTNAME}:${parseInt(process.env.REDIS_PORT)}`;
const redisClient = new Redis({ url: url });

class RedisClient {
    // Function to set data in Redis
    async setCache(key, data) {
        try {
            await redisClient.set(key, JSON.stringify(data));
            console.log('Data set successfully in Redis.');
        } catch (error) {
            console.error('Error setting data in Redis:', error);
            throw error;
        }
    }

    async getCache() {
        try {
            // Get all keys matching a pattern (e.g., "*") to retrieve all data
            const keys = await redisClient.keys('*');

            // Fetch values for each key
            const values = await Promise.all(keys.map(async (key) => {
                return JSON.parse(await redisClient.get(key));
            }));

            return values;
        } catch (error) {
            console.error('Error retrieving data from Redis:', error);
            throw error;
        }
    }

    async getCacheByKey(key) {
        try {
            const value = await redisClient.get(key);
            console.log(`Value for key ${key}:`, value);
            return value;
        } catch (error) {
            console.error('Error getting data:', error);
        }
    }

    async deleteCache(key) {
        try {
            const result = await redisClient.del(key);
            console.log(`Data deleted for key: ${key}`);
            return result;
        } catch (error) {
            console.error('Error deleting data:', error);
        }
    }

    async searchByAccountOrIdentity(accountNumber, identityNumber) {
        try {
            // Get user keys by account number and identity number
            const accountKeys = await redisClient.smembers(`accountNumber:${accountNumber}`);
            const identityKeys = await redisClient.smembers(`identityNumber:${identityNumber}`);

            // Combine keys from both sets
            const combinedKeys = [...new Set([...accountKeys, ...identityKeys])];

            // Fetch user data for each key and filter out non-matching data
            const userData = await Promise.all(combinedKeys.map(async (key) => {
                const user = JSON.parse(await redisClient.get(key));
                if (user.accountNumber === accountNumber || user.identityNumber === identityNumber) {
                    return user;
                }
            }));

            // Remove undefined values (non-matching data)
            const filteredUserData = userData.filter(user => user);

            return filteredUserData;
        } catch (error) {
            console.error('Error fetching user data:', error);
            throw error;
        }
    }

}

module.exports = RedisClient;

