const RedisClient = require('./RedisClient');

class CacheService {
    constructor() {
        this.redisClient = new RedisClient();
    }

    async updateCache(key, data) {
        try {
            // Utilize redisService to insert data into Redis
            await this.redisClient.setCache(key, data);
            console.log('Data inserted into Redis cache successfully.');
        } catch (error) {
            console.error('Error inserting data into cache:', error);
            throw error;
        }
    }

    async getUserFromCache(param1, param2) {
        // Retrieve user data from cache
        const cachedData = await this.redisClient.searchByAccountOrIdentity(param1, param2);
        return cachedData ? JSON.parse(JSON.stringify(cachedData))[0] : null;
    }

    async getUserFromCacheByKey(key) {
        // Retrieve user data from cache
        const cachedData = await this.redisClient.getCacheByKey(key);
        return cachedData ? JSON.parse(cachedData) : null;
    }

    async deleteFromCache(key) {
        // Remove data from cache
        await this.redisClient.deleteCache(key);
    }
}

module.exports = CacheService;
