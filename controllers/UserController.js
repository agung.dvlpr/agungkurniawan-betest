const UserService = require('../services/UserService');

class UserController {
    constructor() {
        this.userService = new UserService();

        // Bind methods to the instance of the controller
        this.createUser = this.createUser.bind(this);
        this.getAllUser = this.getAllUser.bind(this);
        this.getUserById = this.getUserById.bind(this);
        this.updateUser = this.updateUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
    }

    async createUser(req, res) {
        try {
            const user = req.body;
            const createdUser = await this.userService.createUser(user);
            res.status(201).json(createdUser);
        } catch (error) {
            res.status(400).json({ message: error.message });
        }
    }

    async getAllUser(req, res) {
        const accountNumber = (req.query.accountNumber) ? req.query.accountNumber : null;
        const IdentityNumber = (req.query.identityNumber) ? req.query.identityNumber : null;
        try {
            const user = await this.userService.getAllUser(accountNumber, IdentityNumber);
            if (!user) {
                return res.status(404).json({ message: 'User not found' });
            }
            res.json(user);
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    }

    async getUserById(req, res) {
        try {
            const id = req.params.id;
            const user = await this.userService.getUserById(id);
            if (!user) {
                return res.status(404).json({ message: 'User not found' });
            }
            res.json(user);
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    }

    async updateUser(req, res) {
        try {
            const id = req.params.id;
            const userData = req.body;
            const updatedUser = await this.userService.updateUser(id, userData);
            res.json(updatedUser);
        } catch (error) {
            res.status(400).json({ message: error.message });
        }
    }

    async deleteUser(req, res) {
        try {
            const id = req.params.id;
            await this.userService.deleteUser(id);
            res.json({ message: 'User deleted successfully' });
        } catch (error) {
            res.status(400).json({ message: error.message });
        }
    }
}

module.exports = UserController;
