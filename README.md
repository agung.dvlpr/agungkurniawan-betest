# agungkurniawan-betest



## Getting started
-Create microservices (Nodejs - Express) for CRUD operation and store in database
(MongoDB) for user data
    o User data : Id, userName, accountNumber, emailAddress, identityNumber
-Protect the APIs using the authorization header JWT
-Validate authorization header JWT
-Implement cache strategy using Redis for user data.
-Create Unit Test
-Deploy into docker/heroku or any platform services and running
Notes:
    1. Provide API to generate JWT token
    2. Follow OOP coding practises (Design pattern and principles – SOLID) and data structures.
    3. For read operation please create functions to get user by accountNumber and get user by IdentityNumber.
    4. Please define the constraint and indexing keys in database.

# Setting ENV
SECRET_KEY_JWT=vk3hcQZN0cznvS4aikLK3VDt93UmFU2gieNfs1fKXAMZDu0rjSbV9iLltqYZbSUN2024
REDIS_PASSWORD=asd123$
REDIS_HOSTNAME=localhost
REDIS_PORT=6379
REDIS_USERNAME=redis_agungkurniawan_ betest

MONGODB_HOST=localhost
MONGODB_PORT=27017
MONGODB_DATABASE=db_agungkurniawan_betest