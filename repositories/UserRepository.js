const User = require('../models/User');

class UserRepository {
    async createUser(userData) {
        // Implementation to create user in MongoDB
        try {
            const newUser = new User(userData);
            await newUser.save();
            return newUser;
        } catch (error) {
            throw new Error('Error creating user in MongoDB: ' + error.message);
        }
    }

    async getAllUser(param1, param2) {
        // Implementation to get all user from MongoDB
        try {
            const parameter = (param1 && param2) ? { accountNumber: param1, identityNumber: param2 } : {};
            const data = await User.find(parameter);
            return data;
        } catch (error) {
            throw new Error('Error getAllUser user in MongoDB: ' + error.message);
        }

    }

    async getUserById(id) {
        // Implementation to get user by ID from MongoDB
    }

    async getUserByAccountNumber(param) {
        // Implementation to get user by Account Number from MongoDB
        try {
            const data = await User.findOne({ accountNumber: param });
            return data;
        } catch (error) {
            throw new Error('Error getUserByAccountNumber user in MongoDB: ' + error.message);
        }
    }

    async getUserByIdentityNumber(param) {
        // Implementation to get user by Identity Number from MongoDB
        try {
            const data = await User.findOne({ identityNumber: param });
            return data;
        } catch (error) {
            throw new Error('Error getUserByIdentityNumber user in MongoDB: ' + error.message);
        }
    }

    async updateUser(id, userData) {
        // Implementation to update user in MongoDB
        try {
            const updatedUser = await User.findOneAndUpdate({ _id: id }, userData, { new: true });
            if (!updatedUser) {
                throw new Error('User not found');
            }

            return updatedUser;
        } catch (error) {
            throw new Error('Error update user in MongoDB: ' + error.message);
        }
    }

    async deleteUser(id) {
        // Implementation to delete user from MongoDB
        try {
            const deleteUser = await User.deleteOne({ _id: id });
            if (!deleteUser) {
                throw new Error('User not found');
            }

            return deleteUser;
        } catch (error) {
            throw new Error('Error delete user in MongoDB: ' + error.message);
        }
    }
}

module.exports = UserRepository;
