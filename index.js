// index.js
const express = require('express');
const jwt = require('jsonwebtoken');
require('dotenv').config()
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes');

const app = express();

app.use(express.json());

// MongoDB connection
mongoose.connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`, {
    // useNewUrlParser: true,
    // useUnifiedTopology: true
}).then(() => {
    console.log('Connected to MongoDB');
}).catch((err) => {
    console.error('Error connecting to MongoDB:', err);
    process.exit(1);
});

// Routes
app.use('/users', userRoutes);


// Secret key used for signing JWT tokens
const secretKey = process.env.SECRET_KEY_JWT

// Define API endpoint to generate JWT token
app.post('/api/generate-token', (req, res) => {
    // Sample user data (replace this with your authentication logic)
    const user = {
        userName: 'example_user',
        emailAddress: 'user@example.com'
    };

    // Generate JWT token with user payload and secret key
    jwt.sign({ user }, secretKey, { expiresIn: '1h' }, (err, token) => {
        if (err) {
            res.status(500).json({ error: 'Failed to generate token' });
        } else {
            res.json({ token });
        }
    });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});