const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const UserService = require('../services/UserService');
const User = require('../models/User');

chai.use(chaiHttp);
chai.use(sinonChai);
const expect = chai.expect;

describe('User Service', () => {
    let userService;

    beforeEach(() => {
        userService = new UserService();
    });

    it('should create a new user', async () => {
        // Stub UserRepository and CacheService methods
        const dataUser = new User({
            userName: 'John1',
            accountNumber: '1231',
            emailAddress: 'john1@example.com',
            identityNumber: 'ID1231'
        });

        sinon.stub(userService.userRepository, 'createUser').returns(dataUser);
        sinon.stub(userService.cacheService, 'updateCache').resolves();

        const newUser = await userService.createUser(dataUser);

        expect(newUser).to.deep.equal(dataUser);

    });

    // Write similar tests for other UserService methods
});

