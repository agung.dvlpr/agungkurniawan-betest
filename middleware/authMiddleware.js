const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
    const token = req.headers['authorization'];
    if (!token) {
        return res.status(401).json({ message: 'No token provided' });
    }
    jwt.verify(token.split(' ')[1], process.env.SECRET_KEY_JWT, (err, decoded) => {
        if (err) {
            return res.status(403).json({ message: 'Failed to authenticate token' });
        }
        req.accountNumber = decoded.accountNumber;
        next();
    });
};

module.exports = verifyToken;
