const UserRepository = require('../repositories/UserRepository');
const CacheService = require('../cache/CacheService');

class UserService {
    constructor() {
        this.userRepository = new UserRepository();
        this.cacheService = new CacheService();
    }

    async createUser(user) {

        // Create user in MongoDB
        const createdUser = await this.userRepository.createUser(user);

        // Update cache
        await this.cacheService.updateCache(createdUser._id, createdUser);

        return createdUser;
    }

    async getAllUser(param1, param2) {
        const cachedUser = await this.cacheService.getUserFromCache(param1, param2);
        console.log(cachedUser);
        if (cachedUser) {
            return cachedUser;
        }

        // If not in cache, fetch from MongoDB
        const user = await this.userRepository.getAllUser(param1, param2);
        console.log(user);
        // Update cache
        if (user) {
            // await this.cacheService.updateCache(user._id, user);
        }

        return user;
    }

    async getUserById(id) {
        // Check cache first
        const cachedUser = await this.cacheService.getUserFromCacheByKey(id);
        if (cachedUser) {
            return cachedUser;
        }

        // If not in cache, fetch from MongoDB
        const user = await this.userRepository.getUserById(id);

        // Update cache
        if (user) {
            await this.cacheService.updateCache(id, user);
        }

        return user;
    }

    async updateUser(id, userData) {
        // Update user in MongoDB
        const updatedUser = await this.userRepository.updateUser(id, userData);

        // Update cache
        await this.cacheService.updateCache(id, updatedUser);

        return updatedUser;
    }

    async deleteUser(id) {
        // Delete user from MongoDB
        await this.userRepository.deleteUser(id);

        // Remove from cache
        await this.cacheService.deleteFromCache(id);
    }
}

module.exports = UserService;
